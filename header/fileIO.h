/*
 * fileIO.h
 *
 *  Created on: 12.10.2013
 *      Author: Yaro
 */

#ifndef FILEIO_H_
#define FILEIO_H_

#include <inttypes.h>
#include <iostream>
#include <string>
#include <fstream>
#include <vector>

void readFullFile(std::string fileName, std::vector<uint8_t>& data);

#endif /* FILEIO_H_ */
