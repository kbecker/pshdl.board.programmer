/*
 * password.h
 *
 *  Created on: 03.05.2014
 *      Author: Yaro
 */

#ifndef PASSWORD_H_
#define PASSWORD_H_

#include "serial/serial.h"
#include <string.h>

#define USB_PASSWORD "PSHDLBoardUnlock"

void sendUsbPassword(serial::Serial& my_serial);


#endif /* PASSWORD_H_ */
