/*
 * workers.h
 *
 *  Created on: 23.11.2013
 *      Author: Yaro
 */

#ifndef WORKERS_H_
#define WORKERS_H_

#include <iostream>
#include "serial/serial.h"
#include <pthread.h>
#include <vector>
#include <inttypes.h>
#include <unistd.h>
#include "UserInteraction.h"
#include "ThreadSafeQueue.h"
#include "termination.h"

struct outputSerialArguments {
	serial::Serial *my_serial;
	pthread_mutex_t *mutexFinishFlag;
	UserInteraction *user;
};

struct cinBufferdReadArguments {
	ThreadSafeQueue<uint8_t> *cinQueue;
	dataFormat userInputDataFormat;
};

void initQuitMutexFlag(pthread_mutex_t& mutexFinishFlag);
void tellThreadToQuit(pthread_mutex_t& mutexFinishFlag);
void* outputSerial(void *args);

int needQuit(pthread_mutex_t *mtx);

void *cinBufferdRead(void *args);

#endif /* WORKERS_H_ */
