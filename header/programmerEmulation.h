/*
 * programmer.h
 *
 *  Created on: 12.10.2013
 *      Author: Yaro
 */

#ifndef PROGRAMMER_H_
#define PROGRAMMER_H_

#include <inttypes.h>
#include <iostream>
#include <vector>



#define device_SD 4
#define device_rows 2300
#define OUTPUT_BYTE_COUNT_PER_ROW 2398

typedef enum programmerAction {
	/* Action Names -- match actions function */
	/* These codes are passed to the main entry function "dp_top" to indicate
	 * which action to perform */
	DP_NO_ACTION_FOUND = 0u,
	DP_DEVICE_INFO_ACTION_CODE = 1u,
	DP_READ_IDCODE_ACTION_CODE = 2u,
	DP_ERASE_ACTION_CODE = 3u,
	DP_PROGRAM_WITHOUT_VERIFY_ACTION_CODE = 4u,
	DP_PROGRAM_ACTION_CODE = 5u,
	DP_VERIFY_ACTION_CODE = 6u,
	/* Array only actions */
	DP_ENC_DATA_AUTHENTICATION_ACTION_CODE = 7u,
	DP_ERASE_ARRAY_ACTION_CODE = 8u,
	DP_PROGRAM_ARRAY_ACTION_CODE = 9u,
	DP_VERIFY_ARRAY_ACTION_CODE = 10u,
	/* FROM only actions */
	DP_ERASE_FROM_ACTION_CODE = 11u,
	DP_PROGRAM_FROM_ACTION_CODE = 12u,
	DP_VERIFY_FROM_ACTION_CODE = 13u,
	/* Security only actions */
	DP_ERASE_SECURITY_ACTION_CODE = 14u,
	DP_PROGRAM_SECURITY_ACTION_CODE = 15u,
	/* NVM only actions */
	DP_PROGRAM_NVM_ACTION_CODE = 16u,
	DP_VERIFY_NVM_ACTION_CODE = 17u,
	DP_VERIFY_DEVICE_INFO_ACTION_CODE = 18u,
	DP_READ_USERCODE_ACTION_CODE = 19u,
	/* For P1 device, The following two actions are only supported with data files
	 * generated form V86 or later.  ENABLE_V85_DAT_SUPPORT must be disabled */
	DP_PROGRAM_NVM_ACTIVE_ARRAY_ACTION_CODE = 20u,
	DP_VERIFY_NVM_ACTIVE_ARRAY_ACTION_CODE = 21u,
	DP_IS_CORE_CONFIGURED_ACTION_CODE = 22u,
	DP_PROGRAM_PRIVATE_CLIENTS_ACTION_CODE = 23u,
	DP_VERIFY_PRIVATE_CLIENTS_ACTION_CODE = 24u,
	DP_PROGRAM_PRIVATE_CLIENTS_ACTIVE_ARRAY_ACTION_CODE = 25u,
	DP_VERIFY_PRIVATE_CLIENTS_ACTIVE_ARRAY_ACTION_CODE = 26u
} programmerAction;



/****************************************************************************/
/* JTAG states codes used to identify current and target JTAG states        */
/****************************************************************************/
#define JTAG_TEST_LOGIC_RESET	1u
#define JTAG_RUN_TEST_IDLE		2u
#define JTAG_SHIFT_DR		    3u
#define JTAG_SHIFT_IR		    4u
#define JTAG_EXIT1_DR		    5u
#define JTAG_EXIT1_IR		    6u
#define JTAG_PAUSE_DR		    7u
#define JTAG_PAUSE_IR		    8u
#define JTAG_UPDATE_DR		    9u
#define JTAG_UPDATE_IR		    10u
#define JTAG_CAPTURE_DR		    11u

#define TCK     1
#define TDI     2
#define TMS     8
#define TRST    4
#define TDO     16

#define ISC_DATA_SHIFT_CYCLES          3u
#define ARRAY_ROW_LENGTH         26u
#define BTYES_PER_TABLE_RECORD    9u

uint32_t dp_get_bytes(uint32_t byte_index, uint8_t bytes_requested, uint8_t* fpgaImage);
void dp_check_image_crc(uint8_t* fpgaImage);

uint8_t * dp_get_data_block_element_address(uint32_t bit_index, uint8_t* fpgaImage);
uint8_t* dp_get_header_data(uint32_t bit_index, uint8_t* fpgaImage);
uint32_t dp_get_header_bytes(uint32_t byte_index, uint8_t bytes_requested, uint8_t* fpgaImage);
void dp_get_data_block_address(uint8_t requested_var_ID, uint8_t* fpgaImage);
uint8_t* dp_get_data(uint8_t var_ID, uint32_t bit_index, uint8_t* fpgaImage);
void jtag_outp(uint8_t outdata);
void dp_jtag_tms(uint8_t tms);
void dp_jtag_tms_tdi(uint8_t tms, uint8_t tdi);
void dp_jtag_init(void);
void goto_jtag_state(uint8_t target_state, uint8_t cycles);
void dp_shift_in(uint32_t start_bit, uint16_t num_bits, uint8_t tdi_data[], uint8_t terminate);
void dp_get_and_shift_in(uint8_t Variable_ID, uint16_t total_bits_to_shift, uint32_t start_bit_index, uint8_t* fpgaImage);
void dp_get_and_DRSCAN_in(uint8_t Variable_ID, uint16_t total_bits_to_shift, uint32_t start_bit_index, uint8_t* fpgaImage);
void IRSCAN_in(void);
void dp_load_row_data(uint8_t* fpgaImage);
void getFPGAProgrammingSequence(std::vector<uint8_t>& FPGAProgrammingSequence, uint8_t* fpgaImage);

#endif /* PROGRAMMER_H_ */
