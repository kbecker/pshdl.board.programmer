//============================================================================

//#include <iostream>
//
//using namespace std;
//
//int main(int argc, char *argv[]) {
//	try {
//		throw 20;
//	} catch (int e) {
//		cout << "An exception occurred. Exception Nr. " << e << '\n';
//		cout.flush();
//	}
//	return 0;
//}

#include "fileIO.h"
#include <inttypes.h>
#include <iostream>
#include <string>
#include <vector>
#include <iterator>
#include <pthread.h>

//#include "serial/serial.h"
#include "programmerEmulation.h"
#include "UserInteraction.h"
#include "compression.h"
#include "programmerInteractions.h"
#include "dataLinkerInteraction.h"
#include "workers.h"
#include "termination.h"
#include "password.h"
#include "versionInteraction.h"

using namespace std;

uint32_t debug;
uint8_t debugBuf[100000];

int main(int argc, char *argv[]) {
	try {
		UserInteraction user(argc, argv);

		if (user.getSerialPortName() == NULL) {
			throw string("Serial port is not set! (-port is obligatory) \n");
		}

		uint32_t baudrate = 115200; //fake baudrate
		serial::Timeout timeout = serial::Timeout::simpleTimeout(std::numeric_limits<uint32_t>::max() / 4);
		serial::Serial my_serial(user.getSerialPortName(), baudrate, timeout);
		if (!my_serial.isOpen()) {
			cout << "serial port could not be opened" << endl;
			return 2;
		}

		initSigtermHandler(user);

		sendUsbPassword(my_serial);

		switch (user.getNextOperationMode()) {
		case Operation_Mode_PERFORM_PROGRAMMER_ACTION: {
			vector<uint8_t> fpgaImage;

			readFullFile(user.getDatFilePath(), fpgaImage);

			vector<uint8_t> FPGAProgrammingSequence;
			getFPGAProgrammingSequence(FPGAProgrammingSequence, &fpgaImage[0]);

			vector<uint16_t> rowDataCount;
			vector<uint8_t> compressedFPGAProgrammingSequence;
			compress(compressedFPGAProgrammingSequence, rowDataCount, FPGAProgrammingSequence);
			//decompressDebug(compressedFPGAProgrammingSequence, FPGAProgrammingSequence);

			handle_PERFORM_ACTION_command(my_serial, user.getNextProgrammerAction(), &fpgaImage[0], FPGAProgrammingSequence, compressedFPGAProgrammingSequence,
					rowDataCount);
			break;
		}
		case Operation_Mode_USE_DATA_LINKER_SPI: {
			handle_USE_DATA_LINKER_SPI_command(my_serial, user);
			break;
		}
		case Operation_Mode_USE_DATA_LINKER_UART: {
			handle_USE_DATA_LINKER_UART_command(my_serial, user);
			break;
		}
		case Operation_Mode_INFO: {
			handle_INFO_command(my_serial);
			break;
		}
		case Operation_Mode_FINISH:{
			cout << "\n\nUser Program finished!";
			cout.flush();
			break;
		}
		case Operation_Mode_NO_COMMAND:{
			cout << "\n\nNo Operation mode specified! (program, or link)!";
			cout.flush();
			break;
		}
		case Operation_Mode_DEBUG: {
			uint8_t c='\0';
			cout << "debug mode\n";
			while(c != 'q'){
				cin >> c;
				my_serial.write((uint8_t*) &c, 1);
				my_serial.read(&c,1);
				cout << (char)c;
			}
			break;
		}
		default:
			cout << "wrong operation mode";
			cout.flush();
			break;
		}
	} catch (string& s) {
		cout << s;

		cout << "\n\nreadme.txt:\n";
		cout << string((char*) readme_txt, readme_txt_len);

		return 1;
	} catch (serial::IOException &e) {
		cerr << e.what() << "\n" << "Cannot connect to serial port!\nHardware not appropriately connected?" << endl;
		return 1;
	}

	catch (...) {
		cerr << "unknown exception occurred!";

		return 1;
	}

	return 0;
}

