/*
 * programmer.c
 *
 *  Created on: 23.10.2013
 *      Author: Yaro
 */

#include "programmerEmulation.h"

using namespace std;

uint32_t dp_get_bytes(uint32_t byte_index, uint8_t bytes_requested, uint8_t* fpgaImage) {
	uint32_t ret = 0U;

	for (uint8_t i = 0, j = 0; i < bytes_requested; i++) {
		ret |= ((uint32_t) (fpgaImage[byte_index + i])) << (j++ * 8U);
	}

	return ret;
}

//hier m�ssen vllt magic numbers f�r den anderen FPGA ge�ndert werden
void dp_check_image_crc(uint8_t* fpgaImage) {
	uint16_t expected_crc;

	//cout<<"\r\nChecking data CRC...";

	uint32_t temp = dp_get_bytes(0u, 4u, fpgaImage);
	if ((temp == 0x69736544u) || (temp == 0x65746341u) || (temp == 0x2D4D3447u)) {
		uint32_t requested_bytes = 0u;
		uint32_t image_size = dp_get_bytes(25, 4u, fpgaImage);
		expected_crc = (uint16_t) dp_get_bytes(image_size - 2u, 2u, fpgaImage);

		//cout<<"\r\nExpected CRC="<<std::hex<< expected_crc <<std::dec;

		if (image_size == 0u) {
			std::cout << "\r\nData file is not loaded... \r\n";
		} else {
			//cout<<"\r\nCalculating actual CRC...";
			uint16_t crcValue = 0u;

			requested_bytes = image_size - 2u;

			for (uint32_t i = 0; i < requested_bytes; i++) {
				uint8_t crcComputeByte = fpgaImage[i];

				for (uint32_t j = 0; j < 8u; j++) {
					uint16_t testValue = (crcComputeByte ^ crcValue) & 0x01u;
					crcValue >>= 1u;
					if (testValue) {
						crcValue ^= 0x8408u;
					}
					crcComputeByte >>= 1u;
				}
			}

			if (crcValue != expected_crc) {
				std::cout << "\r\nCRC verification failed.  Expected CRC = " << std::hex << crcValue << std::dec;
				std::cout << " Actual CRC = " << std::hex << dp_get_bytes(image_size - 2, 2, fpgaImage) << std::dec << "\r\n";
			}
		}
	} else {
		std::cout << "\r\nData file is not valid. ";
	}

	return;
}







uint8_t opcode;
uint8_t current_jtag_state;
uint8_t global_jtag_i;
uint8_t jtag_port_reg;
uint8_t SDNumber;


uint8_t LCPS_PortOut;
vector<uint8_t> LCPS_PortOutBuffer;



uint32_t current_block_address;
uint8_t current_var_ID;

uint32_t image_size;


uint8_t * dp_get_data_block_element_address(uint32_t bit_index, uint8_t* fpgaImage)
{
    uint32_t image_requested_address;
    /* Calculating the relative address of the data block needed within the image */
    image_requested_address = current_block_address + bit_index / 8U;

    return &fpgaImage[image_requested_address];

}



uint8_t* dp_get_header_data(uint32_t bit_index, uint8_t* fpgaImage)
{
    /* The first block in the image is the header.  There is no need to get the address of its block.  It is zero. */
    current_block_address = 0U;

    /* Calculating the relative address of the data block needed within the image */
    return dp_get_data_block_element_address(bit_index, fpgaImage);
}



uint32_t dp_get_header_bytes(uint32_t byte_index,uint8_t bytes_requested, uint8_t* fpgaImage)
{
    uint32_t ret = 0U;
    uint8_t i;
    uint8_t j=0u;


	uint8_t* page_buffer_ptr = dp_get_header_data(byte_index*8, fpgaImage);


	for (i=0u; i < bytes_requested; i++)
	{
		ret |= (((uint32_t) page_buffer_ptr[i])) << ( j++ * 8U);
	}

    return ret;
}


void dp_get_data_block_address(uint8_t requested_var_ID, uint8_t* fpgaImage)
{
    uint16_t var_idx;
    uint32_t image_index;
    uint16_t num_vars;
    uint8_t variable_ID;

    /* If the current data block ID is the same as the requested one, there is no need to caluclate its starting address.
    *  it is already calculated.
    */

    if (current_var_ID != requested_var_ID)
    {
        current_block_address=0U;
        current_var_ID=0;
        if (requested_var_ID != 0)
        {
            /*The lookup table is at the end of the header*/
            image_index = dp_get_header_bytes(24,1U, fpgaImage);
            image_size = dp_get_header_bytes(25,4U, fpgaImage);


            /* The last byte in the header is the number of data blocks in the dat file */
            num_vars = (uint16_t) dp_get_header_bytes(image_index-1U,1U, fpgaImage);

            for (var_idx=0U;var_idx<num_vars;var_idx++)
            {
                variable_ID = (uint8_t) dp_get_header_bytes(image_index+BTYES_PER_TABLE_RECORD*var_idx,1U, fpgaImage);
                if (variable_ID == requested_var_ID)
                {
                    current_block_address = dp_get_header_bytes(image_index+BTYES_PER_TABLE_RECORD*var_idx+1U,4U, fpgaImage);
                    current_var_ID = variable_ID;
                    break;
                }
            }
        }
    }
    return;
}


uint8_t* dp_get_data(uint8_t var_ID,uint32_t bit_index, uint8_t* fpgaImage)
{
	uint32_t image_requested_address;

	if (var_ID == 0)
	current_block_address = 0;
	else
	dp_get_data_block_address(var_ID, fpgaImage);

	if ((current_block_address ==0) && (var_ID != 0))
	{
		cout<<"something wrong...";
		return NULL;
	}
	/* Calculating the relative address of the data block needed within the image */
	image_requested_address = current_block_address + bit_index / 8;

	return &fpgaImage[image_requested_address];
}



void jtag_outp(uint8_t outdata)
{
	uint8_t temp = LCPS_PortOut;

	temp &= ~ (TCK | TMS | TRST | TDI);

	temp |= outdata & (TCK | TMS | TRST | TDI);

	LCPS_PortOut = temp;

	LCPS_PortOutBuffer.push_back(temp);
}


void dp_jtag_tms(uint8_t tms)
{
	jtag_port_reg &= ~(TMS | TCK);
	jtag_port_reg |= (tms ? TMS : 0);
	jtag_outp(jtag_port_reg);
	jtag_port_reg |= TCK;
	jtag_outp(jtag_port_reg);
}


void dp_jtag_tms_tdi(uint8_t tms, uint8_t tdi)
{
	jtag_port_reg &= ~(TMS | TCK | TDI);
	jtag_port_reg |= ((tms ? TMS : 0) | (tdi ? TDI : 0));
	jtag_outp(jtag_port_reg);
	jtag_port_reg |= TCK;
	jtag_outp(jtag_port_reg);
}







void dp_jtag_init(void)
{
	jtag_port_reg = TCK | TRST;
	jtag_outp(jtag_port_reg);
}


void goto_jtag_state(uint8_t target_state, uint8_t cycles)
{
    uint8_t count = 0u;
    uint8_t tms_bits = 0u;
    if (target_state != current_jtag_state)
    {
        switch (target_state) {

            case JTAG_TEST_LOGIC_RESET:
            dp_jtag_init();
            count = 5u;
            tms_bits = 0x1Fu;
            break;

            case JTAG_SHIFT_DR:
            if ((current_jtag_state == JTAG_TEST_LOGIC_RESET) || (current_jtag_state == JTAG_RUN_TEST_IDLE))
            {
                count = 4u;
                tms_bits = 0x2u;
            }
            else if ((current_jtag_state == JTAG_PAUSE_IR) || (current_jtag_state == JTAG_PAUSE_DR))
            {
                count = 5u;
                tms_bits = 0x7u;
            }
            else
            {
            }
            break;

            case JTAG_SHIFT_IR:
            if ((current_jtag_state == JTAG_TEST_LOGIC_RESET) || (current_jtag_state == JTAG_RUN_TEST_IDLE))
            {
                count = 5u;
                tms_bits = 0x6u;
            }
            else if ((current_jtag_state == JTAG_PAUSE_DR) || (current_jtag_state == JTAG_PAUSE_IR))
            {
                count = 6u;
                tms_bits = 0xfu;
            }
            else if (current_jtag_state == JTAG_UPDATE_DR)
            {
                count = 4u;
                tms_bits = 0x3u;
            }
            else
            {
            }
            break;

            case JTAG_RUN_TEST_IDLE:
            if (current_jtag_state == JTAG_TEST_LOGIC_RESET)
            {
                count = 1u;
                tms_bits = 0x0u;
            }
            else if ((current_jtag_state == JTAG_EXIT1_IR) || (current_jtag_state == JTAG_EXIT1_DR))
            {
                count = 2u;
                tms_bits = 0x1u;
            }
            else if ((current_jtag_state == JTAG_PAUSE_DR) || (current_jtag_state == JTAG_PAUSE_IR))
            {
                count = 3u;
                tms_bits = 0x3u;
            }
            else if (current_jtag_state == JTAG_CAPTURE_DR)
            {
                count = 3u;
                tms_bits = 0x3u;
            }
            else
            {
            }
            break;

            case JTAG_PAUSE_IR:
            if (current_jtag_state == JTAG_EXIT1_IR)
            {
                count = 1u;
                tms_bits = 0x0u;
            }
            break;

            case JTAG_PAUSE_DR:
            if (current_jtag_state == JTAG_EXIT1_DR)
            {
                count = 1u;
                tms_bits = 0x0u;
            }
            else if (current_jtag_state == JTAG_RUN_TEST_IDLE)
            {
                count = 4u;
                tms_bits = 0x5u;
            }
            else
            {
            }
            break;

            case JTAG_UPDATE_DR:
            if ((current_jtag_state == JTAG_EXIT1_DR) || (current_jtag_state == JTAG_EXIT1_IR))
            {
                count = 1u;
                tms_bits = 0x1u;
            }
            else if (current_jtag_state == JTAG_PAUSE_DR)
            {
                count = 2u;
                tms_bits = 0x3u;
            }
            else
            {
            }
            break;

            case JTAG_CAPTURE_DR:
            if (current_jtag_state == JTAG_PAUSE_IR)
            {
                count = 5u;
                tms_bits = 0xeu;
            }
            else
            {
            }
            break;

            default:
            	cout<<"error_code = DPE_JTAG_STATE_NOT_HANDLED";
            break;
        }

        for (global_jtag_i = 0u; global_jtag_i < count; global_jtag_i++)
        {
            dp_jtag_tms(tms_bits&0x1u);
            tms_bits >>= 1u;
        }
        current_jtag_state = target_state;
    }
    for (global_jtag_i=0u; global_jtag_i < cycles; global_jtag_i++)
    {
        dp_jtag_tms(0u);
    }
    return;
}






void dp_shift_in(uint32_t start_bit, uint16_t num_bits, uint8_t tdi_data[], uint8_t terminate)
{
	uint8_t data_buf;

    uint8_t idx = (uint8_t) start_bit >> 3;
    uint8_t bit_buf = 1U << (uint8_t)(start_bit & 0x7U);
    if (tdi_data == (uint8_t*)NULL)
    {
        data_buf = 0U;
    }
    else
    {
        data_buf = tdi_data[idx] >> ((uint8_t)(start_bit & 0x7U));
    }
    if (terminate == 0U)
    {
        num_bits++;
    }
    while (--num_bits)
    {
        dp_jtag_tms_tdi(0U, data_buf&0x1U);
        data_buf >>= 1;
        bit_buf <<= 1;
        if ((bit_buf & 0xffU) == 0U )
        {
            bit_buf = 1U;
            idx++;
            if (tdi_data == (uint8_t*)NULL)
            {
                data_buf = 0U;
            }
            else
            {
                data_buf = tdi_data[idx];
            }
        }
    }
    if (terminate)
    {
        dp_jtag_tms_tdi(1U, data_buf & 0x1U);
        if (current_jtag_state == JTAG_SHIFT_IR)
        {
            current_jtag_state = JTAG_EXIT1_IR;
        }
        else if (current_jtag_state == JTAG_SHIFT_DR)
        {
            current_jtag_state = JTAG_EXIT1_DR;
        }
        else
        {
        }
    }
    return;
}

void dp_get_and_shift_in(uint8_t Variable_ID,uint16_t total_bits_to_shift, uint32_t start_bit_index, uint8_t* fpgaImage)
{
    uint32_t page_start_bit_index;
    uint16_t bits_to_shift;
    uint8_t terminate;
    page_start_bit_index = start_bit_index & 0x7U;

    terminate = 0U;

        uint8_t* page_buffer_ptr = dp_get_data(Variable_ID,start_bit_index, fpgaImage);

		bits_to_shift = total_bits_to_shift;
		terminate = 1U;

        dp_shift_in(page_start_bit_index, bits_to_shift, page_buffer_ptr,terminate);


    return;
}

void dp_get_and_DRSCAN_in(uint8_t Variable_ID,uint16_t total_bits_to_shift, uint32_t start_bit_index, uint8_t* fpgaImage)
{
    goto_jtag_state(JTAG_SHIFT_DR,0u);
    dp_get_and_shift_in(Variable_ID, total_bits_to_shift, start_bit_index, fpgaImage);
    goto_jtag_state(JTAG_PAUSE_DR,0u);
    return;
}


void IRSCAN_in(void)
{
    goto_jtag_state(JTAG_SHIFT_IR,0u);
    dp_shift_in(0u, 8, &opcode, 1u);
    goto_jtag_state(JTAG_PAUSE_IR,0u);
    return;
}

uint32_t DataIndex;

void dp_load_row_data(uint8_t* fpgaImage)
{
	current_jtag_state = JTAG_RUN_TEST_IDLE; //Voraussetzung
	jtag_port_reg = 7;
	LCPS_PortOut = 7; //Voraussetzung

    /* Load one row of FPGA Array data  */
    opcode = 0x89;
    IRSCAN_in();

    for ( SDNumber = 1u; SDNumber <= device_SD; SDNumber++ )
    {
        for ( int i = 1; i <= 8; i++ )
        {
            dp_get_and_DRSCAN_in(5, 26, DataIndex, fpgaImage);
            goto_jtag_state(JTAG_RUN_TEST_IDLE,ISC_DATA_SHIFT_CYCLES);
            DataIndex = DataIndex + ARRAY_ROW_LENGTH;
        }
    }
    return;
}


void getFPGAProgrammingSequence(std::vector<uint8_t>& FPGAProgrammingSequence, uint8_t* fpgaImage){
	dp_check_image_crc(fpgaImage);
	FPGAProgrammingSequence.reserve(device_rows * OUTPUT_BYTE_COUNT_PER_ROW);

	uint32_t old = 0;
	for (int i = 0; i < device_rows; ++i) {
		dp_load_row_data(fpgaImage);
		if(old + 2398 != LCPS_PortOutBuffer.size()){
			cerr << "Problem! Strange data File\n";
			throw string("Problem! Strange data File\n");
		}
		old += 2398;
	}

	FPGAProgrammingSequence.swap(LCPS_PortOutBuffer);
}
