/*
 * termination.c
 *
 *  Created on: 22.12.2013
 *      Author: Yaro
 */

#include "termination.h"

#include "readme.h"

using namespace std;

volatile bool terminateFlag = false;

operationMode nextOperationMode;

void initSigtermHandler(UserInteraction& user) {
	//signal(SIGINT, sigtermHandler);
	signal(SIGINT, sigtermHandler);

	nextOperationMode = user.getNextOperationMode();
}

void sigtermHandler(int signum) {
	//cout << "Interrupt signal (" << signum << ") received.\n";
	terminateFlag = true;

	cout<<"\nTrying to shutdown program softly."<<endl;
	cout<<"If program does not shutdown, press ctrl-c again. You will then have to reset the PSHDL Board to ensure proper operation."<<endl;
	cout<<"Note: There are better ways to terminate the program than ctrl-c! Read the readme."<<endl;

	if(nextOperationMode == Operation_Mode_PERFORM_PROGRAMMER_ACTION){
		cout<<"\nAttention!!!!!!!!!\nTerminating the program while programming the FPGA can lead to destruction of the FPGA!"<<endl;
	}

	cout.flush();
}
